import express from 'express'
import cors from 'cors'
import path from 'path'
import { json, urlencoded } from 'body-parser'

import auth, { currentUser, login } from './auth'

export const app = express()

// Middlewares
app.use(cors())
app.use('/static', express.static(path.join(__dirname, '../public')))
app.use(urlencoded({ extended: false }))
app.use(auth)

// Auth Routes
app.post('/api/login', json(), login)
app.get('/api/user/current', currentUser)

export default app
