import { PrismaClient } from '@prisma/client'
import { compare } from 'bcrypt'
import { Request, RequestHandler } from 'express'
import { SignJWT, jwtVerify } from 'jose'

declare global {
  namespace Express {
    interface User {
      id: number
    }
    interface Request {
      user?: Express.User
    }
  }
}

interface UserJwtPayload {
  id: number // The user id
  iat: number // Issued at
  exp: number // Expire time
}

const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY || 'SUPER SECRET KEY'

const orm = new PrismaClient()

export const login: RequestHandler = async (req, res, next) => {
  const { username, password } = req.body

  try {
    const user = await orm.user.findUnique({ where: { username } })

    if (!user) {
      throw new Error('')
    }

    const isValid = await compare(password, user.password)

    if (!isValid) {
      throw new Error('')
    }

    const token = await new SignJWT({ id: user.id })
      .setProtectedHeader({
        alg: 'HS256',
      })
      .setIssuedAt()
      .setExpirationTime('2h')
      .sign(new TextEncoder().encode(JWT_SECRET_KEY))

    res.json({
      token,
      user: {
        id: user.id,
        usermane: user.username,
      },
    })
  } catch (error) {
    res.sendStatus(401)
  }
}

export const verifyToken = async (req: Request) => {
  const { authorization } = req.headers
  const token = (authorization || '').replace('Bearer ', '')

  try {
    const verified = await jwtVerify(
      token,
      new TextEncoder().encode(JWT_SECRET_KEY)
    )

    return verified.payload as unknown as UserJwtPayload
  } catch (error) {
    throw new Error('Invalid token')
  }
}

const authMiddleware: RequestHandler = async (req, res, next) => {
  try {
    const payload = await verifyToken(req)
    req.user = { id: payload.id }
  } catch (error) {
    // ignore
  } finally {
    next()
  }
}

export default authMiddleware

export const currentUser: RequestHandler = async (req, res, next) => {
  try {
    const userDetails = await orm.user.findUnique({
      where: { id: req.user?.id },
    })

    if (!userDetails) {
      throw new Error('')
    }

    res.json({
      id: userDetails.id,
      username: userDetails.username,
    })
  } catch (error) {
    res.sendStatus(401)
  }
}
