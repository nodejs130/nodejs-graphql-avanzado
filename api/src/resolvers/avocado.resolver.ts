import { Attributes, Avocado, Prisma, PrismaClient } from '@prisma/client'
import { AuthenticationError } from 'apollo-server-express'

export type ResolverParent = unknown

export type ResolverContext = {
  orm: PrismaClient
  user: Express.User | undefined
}

export function findAll(
  parent: unknown,
  {
    skip,
    take,
    where,
  }: { skip?: number; take?: number; where: Prisma.AvocadoWhereInput },
  context: ResolverContext
): Promise<Avocado[]> {
  return context.orm.avocado.findMany({
    include: { attributes: true },
    skip,
    take,
    where,
  })
}

export function findOne(
  parent: unknown,
  { id }: { id: string },
  context: ResolverContext
): Promise<Avocado | null> {
  return context.orm.avocado.findUnique({
    where: {
      id: parseInt(id, 10),
    },
    include: { attributes: true },
  })
}

export const resolver: Record<
  keyof (Avocado & { attributes: Attributes }),
  (parent: Avocado & { attributes: Attributes }) => unknown
> = {
  id: (parent) => parent.id,
  sku: (parent) => parent.sku,
  name: (parent) => parent.name,
  price: (parent) => parent.price,
  image: (parent) => parent.image,
  createdAt: (parent) => parent.createdAt,
  updatedAt: (parent) => parent.updatedAt,
  deletedAt: (parent) => parent.deletedAt,
  attributes: (parent) => ({
    description: parent.attributes.description,
    shape: parent.attributes.shape,
    hardiness: parent.attributes.hardiness,
    taste: parent.attributes.taste,
  }),
}

export function createAvo(
  parent: unknown,
  {
    data,
  }: { data: Pick<Avocado, 'name' | 'price' | 'image' | 'sku'> & Attributes },
  context: ResolverContext
): Promise<Avocado> {
  if (context.user == undefined) {
    throw new AuthenticationError('Unauthenticated request')
  }

  const { name, price, image, sku, ...attributes } = data
  return context.orm.avocado.create({
    data: {
      name,
      price,
      image,
      sku,
      attributes: {
        create: { ...attributes },
      },
    },
    include: { attributes: true },
  })
}
