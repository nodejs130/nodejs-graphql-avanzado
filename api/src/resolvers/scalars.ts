import { GraphQLScalarType, Kind } from 'graphql'

export const DateTime = new GraphQLScalarType({
  name: 'DateTime',
  description: 'Represents a date time object',
  serialize(value) {
    if (value instanceof Date) {
      return value.toISOString() // Convert outgoing Date to ISOString for JSON
    }
    throw new Error("GraphQL Date Scalar serializer expected a 'Date' object")
  },
  parseValue(value) {
    if (typeof value === 'number') {
      return new Date(value) // Convert incoming integer to dates
    }
    throw new Error("GraphQL Date Scalar serializer expected a 'numeber'")
  },
  parseLiteral(ast) {
    if (ast.kind === Kind.INT) {
      return new Date(parseInt(ast.value, 10)) // Convert hard-coded AST string to integer and then ro Date
    }
    return null // Invalid hard-coded value (not an integer)
  },
})
